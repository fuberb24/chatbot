import requests
import random
from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet

SPOONACULAR_API_KEY = '36b6547760d34f6c8f49c0b838f884ae'

class ActionRecommendCuisine(Action):
    def name(self) -> Text:
        return "action_recommend_cuisine"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        cuisines = ["African", "Asian", "American", "British", "Cajun", "Caribbean", "Chinese", "French", "German", "Greek", "Indian", "Irish", "Italian", "Japanese", "Jewish", "Korean", "Mediterranean", "Mexican", "Middle Eastern", "Nordic", "Southern", "Spanish", "Thai", "Vietnamese"]
        recommended_cuisine = random.choice(cuisines)
        
        dispatcher.utter_message(text=f"I recommend you try {recommended_cuisine} cuisine!")
        return []

class ActionRecommendDish(Action):
    def name(self) -> Text:
        return "action_recommend_dish"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        cuisine = tracker.get_slot('cuisine_type')
        
        if not cuisine:
            dispatcher.utter_message(text="Sorry, I didn't get the cuisine type. Could you please repeat?")
            return []

        url = f"https://api.spoonacular.com/recipes/complexSearch?cuisine={cuisine}&apiKey={SPOONACULAR_API_KEY}&number=10"
        response = requests.get(url)
        
        if response.status_code == 200:
            data = response.json()
            if data['results']:
                filtered_dishes = [dish for dish in data['results'] if len(dish['title'].split()) <= 3]

                if len(filtered_dishes) >= 2:
                    dishes = random.sample(filtered_dishes, 2)
                    dish_names = [dish['title'] for dish in dishes]
                    dispatcher.utter_message(text=f"How about trying: {dish_names[0]} or {dish_names[1]}?")
            else:
                dispatcher.utter_message(text=f"Sorry, I couldn't find any dishes for {cuisine} cuisine.")
        else:
            dispatcher.utter_message(text="Sorry, I couldn't connect to the recipe service at the moment.")
        
        return []

class ActionProvideRecipeSteps(Action):
    def name(self) -> Text:
        return "action_provide_recipe_steps"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        # Get the dish name from the slot
        dish = tracker.get_slot('dish_name')
        
        if not dish:
            dispatcher.utter_message(text="Sorry, I didn't get the dish name. Could you please repeat?")
            return []

        # Normalize the dish name case
        dish = dish.title()
        
        # Fetch the recipe details from Spoonacular API based on the dish name
        url = f"https://api.spoonacular.com/recipes/complexSearch?query={dish}&apiKey={SPOONACULAR_API_KEY}&number=1"
        response = requests.get(url)
        
        if response.status_code == 200:
            data = response.json()
            if data['results']:
                recipe_id = data['results'][0]['id']
                recipe_url = f"https://api.spoonacular.com/recipes/{recipe_id}/analyzedInstructions?apiKey={SPOONACULAR_API_KEY}"
                recipe_response = requests.get(recipe_url)
                
                if recipe_response.status_code == 200:
                    recipe_data = recipe_response.json()
                    if recipe_data and len(recipe_data) > 0 and 'steps' in recipe_data[0]:
                        # Extract and send each step individually
                        steps = recipe_data[0]['steps']
                        dispatcher.utter_message(text=f"Here are the steps to make {dish}:")
                        for step in steps:
                            dispatcher.utter_message(text=f"{step['number']}. {step['step']}")
                    else:
                        dispatcher.utter_message(text=f"Sorry, I couldn't find the steps for {dish}.")
                else:
                    dispatcher.utter_message(text="Sorry, I couldn't retrieve the recipe steps at the moment.")
            else:
                dispatcher.utter_message(text=f"Sorry, I couldn't find a recipe for {dish}.")
        else:
            dispatcher.utter_message(text="Sorry, I couldn't connect to the recipe service at the moment.")
        
        return []


class ActionShowPicture(Action):
    def name(self) -> Text:
        return "action_show_picture"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        dish = tracker.get_slot('dish_name')
        
        if not dish:
            dispatcher.utter_message(text="Sorry, I didn't get the dish name. Could you please repeat?")
            return []

        dish = dish.title()  # Normalize case
        
        url = f"https://api.spoonacular.com/recipes/complexSearch?query={dish}&apiKey={SPOONACULAR_API_KEY}&number=1"
        response = requests.get(url)
        
        if response.status_code == 200:
            data = response.json()
            if data['results']:
                image_url = data['results'][0]['image']
                test = requests.get(image_url)
                if test.status_code == 404:
                    dispatcher.utter_message("I am unable to fetch this image off of Spoonacular")
                    dispatcher.utter_message("Can I help you with anything else?")
                    return "action_"
                dispatcher.utter_message(text=f"Here is a picture of {dish}:", image=image_url)
            else:
                dispatcher.utter_message(text=f"Sorry, I couldn't find a picture for {dish}.")
        else:
            dispatcher.utter_message(text="Sorry, I couldn't connect to the image service at the moment.")
        
        return []

class ActionNutritionAnalysis(Action):
    def name(self) -> Text:
        return "action_nutrition_analysis"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        dish = tracker.get_slot('dish_name')
        
        if not dish:
            dispatcher.utter_message(text="Sorry, I didn't get the dish name. Could you please repeat?")
            return []

        dish = dish.title()  # Normalize case
        
        url = f"https://api.spoonacular.com/recipes/complexSearch?query={dish}&apiKey={SPOONACULAR_API_KEY}&number=1"
        response = requests.get(url)
        
        if response.status_code == 200:
            data = response.json()
            if data['results']:
                recipe_id = data['results'][0]['id']
                nutrition_url = f"https://api.spoonacular.com/recipes/{recipe_id}/nutritionWidget.json?apiKey={SPOONACULAR_API_KEY}"
                nutrition_response = requests.get(nutrition_url)
                
                if nutrition_response.status_code == 200:
                    nutrition_data = nutrition_response.json()
                    nutrition_info = f"Calories: {nutrition_data['calories']}, Carbs: {nutrition_data['carbs']}, Fat: {nutrition_data['fat']}, Protein: {nutrition_data['protein']}"
                    dispatcher.utter_message(text=f"Here is the nutrition information for {dish}: {nutrition_info}")
                else:
                    dispatcher.utter_message(text="Sorry, I couldn't retrieve the nutrition information at the moment.")
            else:
                dispatcher.utter_message(text=f"Sorry, I couldn't find a recipe for {dish}.")
        else:
            dispatcher.utter_message(text="Sorry, I couldn't connect to the nutrition service at the moment.")
        
        return []

class ActionCostBreakdown(Action):
    def name(self) -> Text:
        return "action_cost_breakdown"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        dish = tracker.get_slot('dish_name')
        
        if not dish:
            dispatcher.utter_message(text="Sorry, I didn't get the dish name. Could you please repeat?")
            return []

        dish = dish.title()  # Normalize case
        
        url = f"https://api.spoonacular.com/recipes/complexSearch?query={dish}&apiKey={SPOONACULAR_API_KEY}&number=1"
        response = requests.get(url)
        
        if response.status_code == 200:
            data = response.json()
            if data['results']:
                recipe_id = data['results'][0]['id']
                cost_url = f"https://api.spoonacular.com/recipes/{recipe_id}/priceBreakdownWidget.json?apiKey={SPOONACULAR_API_KEY}"
                cost_response = requests.get(cost_url)
                
                if cost_response.status_code == 200:
                    cost_data = cost_response.json()
                    cost_info = f"based on estimations from Spooanacular: Total cost: {str(round(cost_data['totalCost']/100.0, 2))} $, Cost per serving: {str(round(cost_data['totalCostPerServing']/100.0, 2))} $"
                    dispatcher.utter_message(text=f"Here is the cost breakdown for {dish}, {cost_info}")
                else:
                    dispatcher.utter_message(text="Sorry, I couldn't retrieve the cost breakdown at the moment.")
            else:
                dispatcher.utter_message(text=f"Sorry, I couldn't find a recipe for {dish}.")
        else:
            dispatcher.utter_message(text="Sorry, I couldn't connect to the cost service at the moment.")
        
        return []

class ActionCookingTips(Action):
    def name(self) -> Text:
        return "action_cooking_tips"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        dish = tracker.get_slot('dish_name')
        
        if not dish:
            dispatcher.utter_message(text="Sorry, I didn't get the dish name. Could you please repeat?")
            return []

        dish = dish.title()  # Normalize case
        
        url = f"https://api.spoonacular.com/recipes/complexSearch?query={dish}&apiKey={SPOONACULAR_API_KEY}&number=1"
        response = requests.get(url)
        
        if response.status_code == 200:
            data = response.json()
            if data['results']:
                recipe_id = data['results'][0]['id']
                tips_url = f"https://api.spoonacular.com/recipes/{recipe_id}/summary?apiKey={SPOONACULAR_API_KEY}"
                tips_response = requests.get(tips_url)
                
                if tips_response.status_code == 200:
                    tips_data = tips_response.json()
                    tips_info = tips_data['summary']
                    dispatcher.utter_message(text=f"Here are some cooking tips for {dish}: {tips_info}")
                else:
                    dispatcher.utter_message(text="Sorry, I couldn't retrieve the cooking tips at the moment.")
            else:
                dispatcher.utter_message(text=f"Sorry, I couldn't find a recipe for {dish}.")
        else:
            dispatcher.utter_message(text="Sorry, I couldn't connect to the cooking tips service at the moment.")
        
        return []

class ActionRelatedRecipes(Action):
    def name(self) -> Text:
        return "action_related_recipes"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        dish = tracker.get_slot('dish_name')
        
        if not dish:
            dispatcher.utter_message(text="Sorry, I didn't get the dish name. Could you please repeat?")
            return []

        dish = dish.title()  # Normalize case
        
        url = f"https://api.spoonacular.com/recipes/complexSearch?query={dish}&apiKey={SPOONACULAR_API_KEY}&number=1"
        response = requests.get(url)
        
        if response.status_code == 200:
            data = response.json()
            if data['results']:
                recipe_id = data['results'][0]['id']
                related_url = f"https://api.spoonacular.com/recipes/{recipe_id}/similar?apiKey={SPOONACULAR_API_KEY}"
                related_response = requests.get(related_url)
                
                if related_response.status_code == 200:
                    related_data = related_response.json()
                    related_recipes = [recipe['title'] for recipe in related_data]
                    dispatcher.utter_message(text=f"Here are some recipes related to {dish}: {', '.join(related_recipes)}")
                else:
                    dispatcher.utter_message(text="Sorry, I couldn't retrieve the related recipes at the moment.")
            else:
                dispatcher.utter_message(text=f"Sorry, I couldn't find a recipe for {dish}.")
        else:
            dispatcher.utter_message(text="Sorry, I couldn't connect to the related recipes service at the moment.")
        
        return []
    
    class ActionRandomRecipe(Action):
        def name(self) -> Text:
            return "action_recommend_random_dish"

        def run(self, dispatcher: CollectingDispatcher,
                tracker: Tracker,
                domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

            # Fetch a random recipe from Spoonacular API
            url = f"https://api.spoonacular.com/recipes/random?apiKey={SPOONACULAR_API_KEY}&number=1"
            response = requests.get(url)

            if response.status_code == 200:
                data = response.json()
                if data['recipes']:
                    recipe_data = data['recipes'][0]  # Access the first (and only) recipe
                    title = recipe_data['title']
                    analyzed_instructions = recipe_data.get('analyzedInstructions', [])
                    
                    if analyzed_instructions:
                        # Extract and send each step individually
                        steps = analyzed_instructions[0].get('steps', [])
                        dispatcher.utter_message(text=f"Here are the steps to make {title}:")
                        for step in steps:
                            dispatcher.utter_message(text=f"{step['number']}. {step['step']}")
                        # Set the dish name slot
                        return [SlotSet("dish_name", title)]
                    else:
                        dispatcher.utter_message(text=f"Sorry, I couldn't find the steps for {title}.")
                else:
                    dispatcher.utter_message(text="Sorry, I couldn't find a recipe.")
            else:
                dispatcher.utter_message(text="Sorry, I couldn't connect to the recipe service at the moment.")
            
            return []
        
    class ActionListIngredients(Action):
        def name(self) -> Text:
            return "action_list_ingredients"

        def run(self, dispatcher: CollectingDispatcher,
                tracker: Tracker,
                domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
            dish = tracker.get_slot('dish_name')
        
            if not dish:
                dispatcher.utter_message(text="Sorry, I didn't get the dish name. Could you please repeat?")
                return []

            dish = dish.title()  # Normalize case
        
            # Fetch the recipe details from Spoonacular API based on the dish name
            url = f"https://api.spoonacular.com/recipes/complexSearch?query={dish}&apiKey={SPOONACULAR_API_KEY}&number=1"
            response = requests.get(url)
        
            if response.status_code == 200:
                data = response.json()
                if data['results']:
                    recipe_id = data['results'][0]['id']
                    recipe_url = f"https://api.spoonacular.com/recipes/{recipe_id}/information?includeNutrition=false&apiKey={SPOONACULAR_API_KEY}"
                    recipe_response = requests.get(recipe_url)
                
                    if recipe_response.status_code == 200:
                        recipe_data = recipe_response.json()
                        ingredients = recipe_data.get('extendedIngredients', [])
                        if ingredients:
                            ingredients_list = [f"{ingredient['original']}" for ingredient in ingredients]
                            ingredients_text = "\n".join(ingredients_list)
                            dispatcher.utter_message(text=f"Here are the ingredients for {dish}:\n{ingredients_text}")
                        else:
                            dispatcher.utter_message(text=f"Sorry, I couldn't find the ingredients for {dish}.")
                    else:
                        dispatcher.utter_message(text="Sorry, I couldn't retrieve the recipe details at the moment.")
                else:
                    dispatcher.utter_message(text=f"Sorry, I couldn't find a recipe for {dish}.")
            else:
                dispatcher.utter_message(text="Sorry, I couldn't connect to the recipe service at the moment.")
        
            return []



